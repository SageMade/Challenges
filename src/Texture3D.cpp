/*
	Author: Shawn Matthews
	Date:   Feb 7 2018
*/
#include "Texture3D.h"

#include <iostream>
#include <fstream>

#include <stb_image.h>

#include "Logger.h"


	uint8_t* __RepackAtlas(uint8_t* data, int& width, int& height, int& depth, const uint32_t xSlices, const uint32_t ySlices) {
		uint32_t zSlices     = xSlices * ySlices;
		uint32_t sliceWidth  = width / xSlices;
		uint32_t sliceHeight = height / ySlices;
		depth                = zSlices;

		uint32_t sliceSize = sliceWidth * sliceHeight;
		uint32_t volume = sliceSize * depth;
		struct pixel {
			uint8_t R;
			uint8_t G;
			uint8_t B;
			uint8_t A;
		};
		pixel* result = (pixel*)malloc(sizeof(pixel) * volume);
		pixel* input = (pixel*)data;
		uint32_t sourceLoc{ 0 }, xLoc{ 0 }, yLoc{ 0 };
		for (uint32_t iz = 0; iz < depth; iz++) {
			for (uint32_t iy = 0; iy < sliceHeight; iy++) {
				for (uint32_t ix = 0; ix < sliceWidth; ix++) {
					xLoc = (iz % xSlices) * sliceWidth + ix;
					yLoc = (iz / ySlices) * sliceHeight + iy;
					result[iz * sliceSize + iy * sliceWidth + ix] = input[yLoc * width + xLoc];
				}
			}
		}
		free(data);

		width = sliceWidth;
		height = sliceHeight;

		return (uint8_t*)result;
	}

	bool endsWith(std::string const &fullString, std::string const &ending) {
		if (fullString.length() >= ending.length()) {
			return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
		}
		else {
			return false;
		}
	}

	Texture3DDefinition::Texture3DDefinition(
		const uint32_t width, const uint32_t height, const uint32_t depth,
		uint8_t * data, const GLenum filterMode,
		const GLenum internalPixelType,
		const GLenum pixelFormat,
		const GLenum pixelType,
		const GLenum wrapMode,
		const bool enableAnisotropy) :
		Width(width), Height(height), Depth(depth), Data(data), FilterMode(filterMode), InternalPixelType(internalPixelType),
		PixelFormat(pixelFormat), PixelType(pixelType), WrapMode(wrapMode), EnableAnisotropy(enableAnisotropy) {}

	Texture3DDefinition::Texture3DDefinition(const std::string & filename, int xSlices, int ySlices) : Texture3DDefinition() {

		if (endsWith(filename, ".cube")) {
			std::ifstream inFile(filename);

			uint8_t* textureData = nullptr;
			uint32_t lutSize{ 0 };
			uint32_t ix{ 0 };
			float r{ 0 }, g{ 0 }, b{ 0 };

			std::string line;
			while (std::getline(inFile, line)) {
				if (line.find("LUT_3D_SIZE") != std::string::npos) {
					std::stringstream lReader(line);
					lReader >> lutSize;
					textureData = (uint8_t*)malloc(lutSize * 3);
				}
				else if (line.find("TITLE") != std::string::npos) {

				}
				else {
					std::stringstream lReader(line);
					lReader >> r >> g >> b;
					textureData[ix++] = r * 255;
					textureData[ix++] = g * 255;
					textureData[ix++] = b * 255;

				}
			}

			Data = textureData;
			PixelFormat = GL_RGB;
			InternalPixelType = GL_RGB8;
			PixelType = GL_UNSIGNED_BYTE;

			Width  = lutSize;
			Height = lutSize;
			Depth  = lutSize;
		}
		else if (endsWith(filename, ".ccube")) {

		}
		else {
			int width{ 0 }, height{ 0 }, depth{ 0 }, numChannels{ 0 };
			// Simple enough, just use the stbi function, the important notes are that we are forcing 4 bytes of pixel data
			Data = stbi_load(filename.c_str(), &width, &height, &numChannels, 4);

			if ((width % xSlices) & (height % ySlices)) {
				stbi_image_free(Data);
				throw new std::runtime_error("Cannot slice, would cause invalid borders");
			}

			Data = __RepackAtlas(Data, width, height, depth, xSlices, ySlices);

			Width = (uint32_t)width;
			Height = (uint32_t)height;
			Depth = depth;
		}
	}

	Texture3DDefinition::~Texture3DDefinition() {
		free(Data);
	}


    // Texture destructor
    Texture3D::~Texture3D()
    {
        // We want to delete our texture from the GPU
        glDeleteTextures(1, &myTextureHandle);
        FILE_LOG(logINFO)  << "Destroying texture: " << myTextureHandle;
    }

    // Implementation of bind
    void Texture3D::Bind(uint8_t textureSlot) {
        // Set the active slot to the given slot
        glActiveTexture(GL_TEXTURE0 + textureSlot);
        // Binds our texture
        glBindTexture(GL_TEXTURE_3D, myTextureHandle);  
    }


    // Implementation of load
    Texture3D::Texture3D(const std::string& filename, int xSlices, int ySlices, GLenum filterMode, GLenum wrapMode, bool enableAnisotropy) {

        FILE_LOG(logINFO) << "Loading texture from " << filename;
		
		if (endsWith(filename, ".cube")) {
			std::ifstream inFile(filename);

			uint8_t* textureData = nullptr;
			int lutSize{ 0 };
			uint32_t ix{ 0 };
			float r, g, b;

			std::string line;
			while (std::getline(inFile, line)) {
				if (line.find("LUT_3D_SIZE") != std::string::npos) {
					std::stringstream lReader(line);
					std::string temp;
					lReader >> temp >> lutSize;
					textureData = new uint8_t[lutSize * lutSize * lutSize * 3];
				}
				else if (line.find("TITLE") != std::string::npos) {

				}
				else if (line.size() > 0){
					std::stringstream lReader(line);
					lReader >> r >> g >> b;
					textureData[ix++] = r * 255;
					textureData[ix++] = g * 255;
					textureData[ix++] = b * 255;

				}
			}

			// If we did, make a texture with it, using GL_RGBA and unsigned bytes
			__Create(lutSize, lutSize, lutSize, textureData, filterMode, GL_RGB8, GL_RGB, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, enableAnisotropy);
			delete[] textureData;
		}
		else if (endsWith(filename, ".ccube")) {

		}
		else {
			// Declare outputs for the stbi function
			int width{ 0 }, height{ 0 }, numChannels{ 0 }, depth{ 0 };
			// Simple enough, just use the stbi function, the important notes are that we are forcing 4 bytes of pixel data
			unsigned char *data = stbi_load(filename.c_str(), &width, &height, &numChannels, 4);

			if ((width % xSlices) & (height % ySlices)) {
				stbi_image_free(data);
				throw new std::runtime_error("Cannot slice, would cause invalid borders");
			}

			data = __RepackAtlas(data, width, height, depth, xSlices, ySlices);

			// Check to see if we got any data
			if (data) {
				// If we did, make a texture with it, using GL_RGBA and unsigned bytes
				__Create((uint32_t)width, (uint32_t)height, (uint32_t)depth, data, filterMode, GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, wrapMode, enableAnisotropy);
				delete data;
			}
			// Otherwise throw an exception
			else {
				throw std::runtime_error("Failed to load texture");
			}
		}

    }
	
    // Implement the create function
	Texture3D::Texture3D(const uint32_t width, const uint32_t height, const uint32_t depth, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy)
    {

        __Create(width, height, depth, data, filterMode, internalPixelType, pixelFormat, pixelType, wrapMode, enableAnisotropy);
    }

	// Implement the create function
	Texture3D::Texture3D(const Texture3DDefinition& definition)
	{
		__Create(definition.Width, definition.Height, definition.Depth, definition.Data, definition.FilterMode, 
			definition.InternalPixelType, definition.PixelFormat, definition.PixelType, definition.WrapMode, definition.EnableAnisotropy);
	}

	void Texture3D::Unload() {
		glDeleteTextures(1, &myTextureHandle);
		myTextureHandle = 0;
		myWidth = 0;
		myHeight = 0;
		myDepth = 0;
	}

    // Actually creates the texture
    void Texture3D::__Create(const uint32_t width, const uint32_t height, const uint32_t depth, uint8_t *data,
                            const GLenum filterMode,
                            const GLenum internalPixelType,
                            const GLenum pixelFormat,
                            const GLenum pixelType,
                            const GLenum wrapMode,
                            const bool   enableAnisotropy) {

        myWidth = width;
        myHeight = height;
		myDepth = depth;

        // Generate a texture for the reuslt
        glGenTextures(1, &myTextureHandle);

        // If we could not generate a texture, throw an exception
        if (myTextureHandle == 0)
            throw new std::runtime_error("Cannot create texture instance");

		glActiveTexture(GL_TEXTURE0);
        // Bind the texture for creation
        glBindTexture(GL_TEXTURE_3D, myTextureHandle);

        // Buffer our texture data
        glTexImage3D(GL_TEXTURE_3D, 0, internalPixelType, width, height, depth, 0, pixelFormat, pixelType, data);

        // Generate mipmaps for LOD stuff and whatnots
        glGenerateMipmap(GL_TEXTURE_3D);

		if (enableAnisotropy) {
			float maxAnisotropy;
			glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAnisotropy);

			glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAnisotropy);
		}
		
        // We want to clamp our texture coords to within the texture
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, wrapMode);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, wrapMode);
		glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, wrapMode);
		
		GLenum magFilter = filterMode;

		switch (filterMode) {
			case GL_LINEAR_MIPMAP_LINEAR:
			case GL_LINEAR_MIPMAP_NEAREST:
				magFilter = GL_LINEAR;
				break;
			case GL_NEAREST_MIPMAP_LINEAR:
			case GL_NEAREST_MIPMAP_NEAREST:
				magFilter = GL_NEAREST;
				break;
		}

        // Set the filter mode
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, filterMode);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, magFilter);

        FILE_LOG(logINFO)  << width << " x " << height << "x" << depth << " @ " << myTextureHandle;
    }

	void Texture3D::Unbind(const GLenum location) {
		glActiveTexture(GL_TEXTURE0 + location);
		glBindTexture(GL_TEXTURE_3D, 0);
	}
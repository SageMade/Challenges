#version 420

uniform vec4 u_lightPos;
uniform vec4 u_colour;

// Note: Uniform bindings
// This lets you specify the texture unit directly in the shader!
layout(binding = 0) uniform sampler2D u_rgb; // rgb texture


// Fragment Shader Inputs
in VertexData
{
	vec3 normal;
	vec3 texCoord;
	vec4 colour;
	vec3 posEye;
} vIn;

// Multiple render targets!
layout(location = 0) out vec4 FragColor;

vec3 diffuse()
{
	vec3 L = normalize(u_lightPos.xyz - vIn.posEye);
	vec3 N = normalize(vIn.normal);

	float ndotl = max(0.0, dot(N, L));
	float diffuseIntensity = 1.0;
	vec3 diffuseColor = texture(u_rgb, vIn.texCoord.xy).rgb;

	if (length(diffuseColor) == 0)
		diffuseColor = vec3(0.5);

	vec3 diffuse = diffuseIntensity * ndotl * diffuseColor;

	return diffuse;
}

void main()
{
	FragColor = vec4(diffuse() + u_colour.rgb, 1.0);
}